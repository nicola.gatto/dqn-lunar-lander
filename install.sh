#!/bin/bash
. config.sh

GENERATOR_PATH="bin/embedded-montiarc-math-middleware-generator-0.0.26-SNAPSHOT-jar-with-dependencies.jar"
AGENT_BUILD="target/agent/build"

rm -rf target

echo "Generate agent..."
java -jar ${GENERATOR_PATH} config/agent.json

echo "Building..."
rm -rf ${BINARY}
rm -rf ${AGENT_BUILD}

mkdir "${BINARY}"

echo "Build agent..."
cmake -B${AGENT_BUILD} -Htarget/agent/src
make -C${AGENT_BUILD}
cp "${AGENT_BUILD}/lander_agent_master/coordinator/Coordinator_lander_agent_master" "${BINARY}/agent"