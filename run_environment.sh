#!/bin/bash
. config.sh

echo "Start ROSCORE..."
xterm -title "ROSCORE" -e "roscore; bash" &
sleep 2

echo "Start up environment..." 
xterm -title "Gym-Environment" -e "${EXEC_ENVIRONMENT_TRAINING}; bash" &
sleep 2